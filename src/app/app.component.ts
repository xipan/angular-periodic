import { Component } from '@angular/core';
import { ElementlistService } from './elementlist.service'
import { Element } from './element'
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'angular-periodic';
  elemnetOfList: any;
  lanthanoidsOfList: any;
  actinideOfList: any;

  constructor(private eService: ElementlistService) {
    this.elemnetOfList = eService.Elements;
    this.lanthanoidsOfList = eService.Lanthanoids;
    this.actinideOfList = eService.Actinide;
  }

  ngOnInit() {
    console.log('hello app component');
    console.log(this.elemnetOfList);
  }
}
