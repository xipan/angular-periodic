import { Injectable } from '@angular/core';
import { Element } from './element'

@Injectable({
  providedIn: 'root'
})
export class ElementlistService {
  Elements = [
    {
      AtomicNumber: '1',
      Symbol: 'H',
      Name: 'Hydrogen',
      AtomicWeight: '1.008',
      ElectronShell: [1],
      Trend: 'other-nonmetals'
    },
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {
      AtomicNumber: '2',
      Symbol: 'He',
      Name: 'Helium',
      AtomicWeight: '4.0026',
      ElectronShell: [2],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '3',
      Symbol: 'Li',
      Name: 'Lithium',
      AtomicWeight: '6.94',
      ElectronShell: [2,1],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '4',
      Symbol: 'Be',
      Name: 'Beryllium',
      AtomicWeight: '9.0122',
      ElectronShell: [2, 2],
      Trend: 'alkaline-earth-metal'
    },
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {
      AtomicNumber: '5',
      Symbol: 'B',
      Name: 'Boron',
      AtomicWeight: '10.81',
      ElectronShell: [2, 3],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '6',
      Symbol: 'C',
      Name: 'Carbon',
      AtomicWeight: '12.011',
      ElectronShell: [2, 4],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '7',
      Symbol: 'N',
      Name: 'Nitrogen',
      AtomicWeight: '14.007',
      ElectronShell: [2, 5],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '8',
      Symbol: 'O',
      Name: 'Oxygen',
      AtomicWeight: '15.999',
      ElectronShell: [2, 6],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '9',
      Symbol: 'F',
      Name: 'Fluorine',
      AtomicWeight: '18.998',
      ElectronShell: [2, 7],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '10',
      Symbol: 'Ne',
      Name: 'Neon',
      AtomicWeight: '20.180',
      ElectronShell: [2,8],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '11',
      Symbol: 'Na',
      Name: 'Sodium',
      AtomicWeight: '22.990',
      ElectronShell: [2, 8],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '12',
      Symbol: 'Mg',
      Name: 'Magnesium',
      AtomicWeight: '24.305',
      ElectronShell: [2, 8,2],
      Trend: 'alkaline-earth-metal'
    },
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    {
      AtomicNumber: '13',
      Symbol: 'AI',
      Name: 'Aluminium',
      AtomicWeight: '26.982',
      ElectronShell: [2, 8, 3],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '14',
      Symbol: 'Si',
      Name: 'Silicon',
      AtomicWeight: '28.085',
      ElectronShell: [2, 8, 4],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '15',
      Symbol: 'P',
      Name: 'Phosphorus',
      AtomicWeight: '30.974',
      ElectronShell: [2, 8, 5],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '16',
      Symbol: 'S',
      Name: 'Sulfur',
      AtomicWeight: '32.06',
      ElectronShell: [2, 8, 6],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '17',
      Symbol: 'CI',
      Name: 'Chlorine',
      AtomicWeight: '35.45',
      ElectronShell: [2, 8, 7],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '18',
      Symbol: 'Ar',
      Name: 'Argon',
      AtomicWeight: '39.848',
      ElectronShell: [2, 8, 8],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '19',
      Symbol: 'K',
      Name: 'Potassium',
      AtomicWeight: '39.098',
      ElectronShell: [2, 8, 8,1],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '20',
      Symbol: 'Ca',
      Name: 'Calcium',
      AtomicWeight: '40.078',
      ElectronShell: [2, 8, 8, 2],
      Trend: 'alkaline-earth-metal'
    },
    {
      AtomicNumber: '21',
      Symbol: 'Sc',
      Name: 'Scandium',
      AtomicWeight: '44.956',
      ElectronShell: [2, 8, 9, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '22',
      Symbol: 'Ti',
      Name: 'Titanium',
      AtomicWeight: '47.867',
      ElectronShell: [2, 8, 10, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '23',
      Symbol: 'V',
      Name: 'Vanadium',
      AtomicWeight: '50.942',
      ElectronShell: [2, 8, 11, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '24',
      Symbol: 'Cr',
      Name: 'Chromium',
      AtomicWeight: '51.996',
      ElectronShell: [2, 8, 13, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '25',
      Symbol: 'Mn',
      Name: 'Manganese',
      AtomicWeight: '54.938',
      ElectronShell: [2, 8, 13, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '26',
      Symbol: 'Fe',
      Name: 'Iron',
      AtomicWeight: '55.845',
      ElectronShell: [2, 8, 14, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '27',
      Symbol: 'Co',
      Name: 'Cobalt',
      AtomicWeight: '58.933',
      ElectronShell: [2, 8, 15, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '28',
      Symbol: 'Ni',
      Name: 'Nickel',
      AtomicWeight: '58.693',
      ElectronShell: [2, 8, 16, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '29',
      Symbol: 'Cu',
      Name: 'Copper',
      AtomicWeight: '63.546',
      ElectronShell: [2, 8, 18, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '30',
      Symbol: 'Zn',
      Name: 'Zinc',
      AtomicWeight: '63.38',
      ElectronShell: [2, 8, 18, 2],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '31',
      Symbol: 'Ga',
      Name: 'Gallium',
      AtomicWeight: '69.723',
      ElectronShell: [2, 8, 18, 3],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '32',
      Symbol: 'Ge',
      Name: 'Gemanium',
      AtomicWeight: '72.630',
      ElectronShell: [2, 8, 18, 4],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '33',
      Symbol: 'As',
      Name: 'Arsenic',
      AtomicWeight: '74.922',
      ElectronShell: [2, 8, 18, 5],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '34',
      Symbol: 'Se',
      Name: 'Selenium',
      AtomicWeight: '78.971',
      ElectronShell: [2, 8, 18, 6],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '35',
      Symbol: 'Br',
      Name: 'Bromine',
      AtomicWeight: '79.904',
      ElectronShell: [2, 8, 18, 7],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '36',
      Symbol: 'Kr',
      Name: 'Krypton',
      AtomicWeight: '83.798',
      ElectronShell: [2, 8, 18, 8],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '37',
      Symbol: 'Rb',
      Name: 'Rubidum',
      AtomicWeight: '85.468',
      ElectronShell: [2, 8, 18, 8,1],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '38',
      Symbol: 'Sr',
      Name: 'Strontium',
      AtomicWeight: '87.62',
      ElectronShell: [2, 8, 18, 8, 2],
      Trend: 'alkaline-earth-metal'
    },
    {
      AtomicNumber: '39',
      Symbol: 'Y',
      Name: 'Yttrium',
      AtomicWeight: '88.906',
      ElectronShell: [2, 8, 18, 9, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '40',
      Symbol: 'Zr',
      Name: 'Zirconium',
      AtomicWeight: '91.224',
      ElectronShell: [2, 8, 18, 10, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '41',
      Symbol: 'Nb',
      Name: 'Niobium',
      AtomicWeight: '92.906',
      ElectronShell: [2, 8, 18, 12, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '42',
      Symbol: 'Mo',
      Name: 'Molybdenur',
      AtomicWeight: '95.95',
      ElectronShell: [2, 8, 13, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '43',
      Symbol: 'Tc',
      Name: 'Technelium',
      AtomicWeight: '98',
      ElectronShell: [2, 8, 18, 13, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '44',
      Symbol: 'Ru',
      Name: 'Ruthenium',
      AtomicWeight: '101.07',
      ElectronShell: [2, 8, 18, 15, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '45',
      Symbol: 'Rh',
      Name: 'Rhodium',
      AtomicWeight: '102.91',
      ElectronShell: [2, 8, 18, 16, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '46',
      Symbol: 'Pd',
      Name: 'Palladium',
      AtomicWeight: '106.42',
      ElectronShell: [2, 8, 18, 18],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '47',
      Symbol: 'Ag',
      Name: 'Sliver',
      AtomicWeight: '107.87',
      ElectronShell: [2, 8, 18, 18, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '48',
      Symbol: 'Cd',
      Name: 'Cadmium',
      AtomicWeight: '112.41',
      ElectronShell: [2, 8, 18, 18, 2],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '49',
      Symbol: 'In',
      Name: 'Indium',
      AtomicWeight: '114.82',
      ElectronShell: [2, 8, 18, 18, 9],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '50',
      Symbol: 'Sn',
      Name: 'Tin',
      AtomicWeight: '118.71',
      ElectronShell: [2, 8, 18, 18, 4],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '51',
      Symbol: 'Sb',
      Name: 'Antimony',
      AtomicWeight: '121.76',
      ElectronShell: [2, 8, 18, 18, 5],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '52',
      Symbol: 'Te',
      Name: 'Tellurium',
      AtomicWeight: '127.60',
      ElectronShell: [2, 8, 18, 18, 6],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '53',
      Symbol: 'I',
      Name: 'Iodine',
      AtomicWeight: '126.90',
      ElectronShell: [2, 8, 18, 18, 7],
      Trend: 'other-nonmetals'
    },
    {
      AtomicNumber: '54',
      Symbol: 'Xe',
      Name: 'Xenon',
      AtomicWeight: '131.29',
      ElectronShell: [2, 8, 18, 18, 8],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '55',
      Symbol: 'Cs',
      Name: 'Casesium',
      AtomicWeight: '132.91',
      ElectronShell: [2, 8, 18, 18, 8, 1],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '56',
      Symbol: 'Ba',
      Name: 'Barium',
      AtomicWeight: '137.33',
      ElectronShell: [2, 8, 18, 18, 8, 2],
      Trend: 'alkaline-earth-metal'
    },
    {
      AtomicNumber: '&nbsp;&nbsp;',
      Symbol: '&nbsp;',
      Name: '57-71',
      AtomicWeight: '&nbsp;',
      ElectronShell: [],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '72',
      Symbol: 'Hf',
      Name: 'Hafnium',
      AtomicWeight: '178.49',
      ElectronShell: [2, 8, 18, 32, 10, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '73',
      Symbol: 'Ta',
      Name: 'Tantalum',
      AtomicWeight: '180.95',
      ElectronShell: [2, 8, 18, 32, 11, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '74',
      Symbol: 'W',
      Name: 'Tungsten',
      AtomicWeight: '183.84',
      ElectronShell: [2, 8, 18, 32, 12, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '75',
      Symbol: 'Re',
      Name: 'Rhenium',
      AtomicWeight: '186.21',
      ElectronShell: [2, 8, 18, 32, 13, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '76',
      Symbol: 'Os',
      Name: 'Osmium',
      AtomicWeight: '190.23',
      ElectronShell: [2, 8, 18, 32, 14, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '77',
      Symbol: 'Ir',
      Name: 'Iridium',
      AtomicWeight: '192.22',
      ElectronShell: [2, 8, 18, 32, 15, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '78',
      Symbol: 'Pt',
      Name: 'Platinum',
      AtomicWeight: '195.08',
      ElectronShell: [2, 8, 18, 32, 17, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '79',
      Symbol: 'Au',
      Name: 'Gold',
      AtomicWeight: '196.97',
      ElectronShell: [2, 8, 18, 32, 18, 1],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '80',
      Symbol: 'Hg',
      Name: 'Mercury',
      AtomicWeight: '200.59',
      ElectronShell: [2, 8, 18, 32, 18, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '81',
      Symbol: 'TI',
      Name: 'Thallium',
      AtomicWeight: '204.38',
      ElectronShell: [2, 8, 18, 32, 18, 3],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '82',
      Symbol: 'Pb',
      Name: 'Lead',
      AtomicWeight: '207.2',
      ElectronShell: [2, 8, 18, 32, 18, 4],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '83',
      Symbol: 'Bi',
      Name: 'Bismuth',
      AtomicWeight: '208.98',
      ElectronShell: [2, 8, 18, 32, 18, 5],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '84',
      Symbol: 'Po',
      Name: 'Polonium',
      AtomicWeight: '209',
      ElectronShell: [2, 8, 18, 32, 18, 6],
      Trend: 'post-​transition-metal'
    },
    {
      AtomicNumber: '85',
      Symbol: 'At',
      Name: 'Astatine',
      AtomicWeight: '210',
      ElectronShell: [2, 8, 18, 32, 18, 7],
      Trend: 'metalloid'
    },
    {
      AtomicNumber: '86',
      Symbol: 'Rn',
      Name: 'Radon',
      AtomicWeight: '222',
      ElectronShell: [2, 8, 18, 32, 18, 8],
      Trend: 'noble-gases'
    },
    {
      AtomicNumber: '87',
      Symbol: 'Fr',
      Name: 'Francium',
      AtomicWeight: '223',
      ElectronShell: [2, 8, 18, 32, 18, 8, 1],
      Trend: 'alkali-metal'
    },
    {
      AtomicNumber: '88',
      Symbol: 'Ra',
      Name: 'Radium',
      AtomicWeight: '226',
      ElectronShell: [2, 8, 18, 32, 18, 8, 2],
      Trend: 'alkaline-earth-metal'
    },
    {
      AtomicNumber: '&nbsp;&nbsp;',
      Symbol: '&nbsp;',
      Name: '89-103',
      AtomicWeight: '&nbsp;',
      ElectronShell: [],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '104',
      Symbol: 'Rf',
      Name: 'Rutherfordiu',
      AtomicWeight: '267',
      ElectronShell: [2, 8, 18, 32, 32, 10, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '105',
      Symbol: 'Db',
      Name: 'Dubnium',
      AtomicWeight: '268',
      ElectronShell: [2, 8, 18, 32, 32, 11, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '106',
      Symbol: 'Sg',
      Name: 'Seaborgium',
      AtomicWeight: '269',
      ElectronShell: [2, 8, 18, 32, 32, 12, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '107',
      Symbol: 'Bh',
      Name: 'Bohrium',
      AtomicWeight: '270',
      ElectronShell: [2, 8, 18, 32, 32, 13, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '108',
      Symbol: 'Hs',
      Name: 'Hassium',
      AtomicWeight: '270',
      ElectronShell: [2, 8, 18, 32, 32, 14, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '109',
      Symbol: 'Mt',
      Name: 'Meitnerium',
      AtomicWeight: '278',
      ElectronShell: [2, 8, 18, 32, 32, 15, 2],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '110',
      Symbol: 'Ds',
      Name: 'Damstadtium',
      AtomicWeight: '281',
      ElectronShell: [2, 8, 18, 32, 32, 17, 1],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '111',
      Symbol: 'Rg',
      Name: 'Roentgenium',
      AtomicWeight: '282',
      ElectronShell: [2, 8, 18, 32, 32, 17, 2],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '112',
      Symbol: 'Cn',
      Name: 'Copemicium',
      AtomicWeight: '285',
      ElectronShell: [2, 8, 18, 32, 32, 18, 2],
      Trend: 'transition-metal'
    },
    {
      AtomicNumber: '113',
      Symbol: 'Nh',
      Name: 'Nihonium',
      AtomicWeight: '286',
      ElectronShell: [2, 8, 18, 32, 32, 18, 3],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '114',
      Symbol: 'FI',
      Name: 'Flerovium',
      AtomicWeight: '289',
      ElectronShell: [2, 8, 18, 32, 32, 18, 5],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '115',
      Symbol: 'Mc',
      Name: 'Moscovium',
      AtomicWeight: '290',
      ElectronShell: [2, 8, 18, 32, 32, 18, 5],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '116',
      Symbol: 'Lv',
      Name: 'Livermorium',
      AtomicWeight: '293',
      ElectronShell: [2, 8, 18, 32, 32, 18, 3],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '117',
      Symbol: 'Ts',
      Name: 'Tennessine',
      AtomicWeight: '294',
      ElectronShell: [2, 8, 18, 32, 32, 18, 7],
      Trend: 'unknown'
    },
    {
      AtomicNumber: '118',
      Symbol: 'Os',
      Name: 'Oganesson',
      AtomicWeight: '294',
      ElectronShell: [2, 8, 18, 32, 32, 18, 8],
      Trend: 'unknown'
    },
  ];

  Lanthanoids: Element[] = [
    {
      AtomicNumber: '57',
      Symbol: 'La',
      Name: 'Lanthanum',
      AtomicWeight: '138.91',
      ElectronShell: [2, 8, 18, 18, 9, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '58',
      Symbol: 'Ce',
      Name: 'Cerium',
      AtomicWeight: '140.12',
      ElectronShell: [2, 8, 18, 19, 9, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '59',
      Symbol: 'Pr',
      Name: 'Praseodymium',
      AtomicWeight: '140.91',
      ElectronShell: [2, 8, 18, 21, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '60',
      Symbol: 'Nd',
      Name: 'Neodymium',
      AtomicWeight: '144.24',
      ElectronShell: [2, 8, 18, 22, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '61',
      Symbol: 'Pm',
      Name: 'Promethium',
      AtomicWeight: '145',
      ElectronShell: [2, 8, 18, 23, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '62',
      Symbol: 'Sm',
      Name: 'Samarium',
      AtomicWeight: '150.36',
      ElectronShell: [2, 8, 18, 24, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '63',
      Symbol: 'Eu',
      Name: 'Europium',
      AtomicWeight: '151.96',
      ElectronShell: [2, 8, 18, 25, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '64',
      Symbol: 'Gd',
      Name: 'Gadolinium',
      AtomicWeight: '157.25',
      ElectronShell: [2, 8, 18, 25, 9, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '65',
      Symbol: 'Tb',
      Name: 'Terbium',
      AtomicWeight: '158.93',
      ElectronShell: [2, 8, 18, 27, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '66',
      Symbol: 'Dy',
      Name: 'Dysprosium',
      AtomicWeight: '162.50',
      ElectronShell: [2, 8, 18, 28, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '67',
      Symbol: 'Ho',
      Name: 'Holmium',
      AtomicWeight: '164.93',
      ElectronShell: [2, 8, 18, 29, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '68',
      Symbol: 'Er',
      Name: 'Erbium',
      AtomicWeight: '167.26',
      ElectronShell: [2, 8, 18, 30, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '69',
      Symbol: 'Tm',
      Name: 'Thulium',
      AtomicWeight: '168.93',
      ElectronShell: [2, 8, 18, 31, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '70',
      Symbol: 'Yb',
      Name: 'Ytterbium',
      AtomicWeight: '173.05',
      ElectronShell: [2, 8, 18, 32, 8, 2],
      Trend: 'lanthanoids'
    },
    {
      AtomicNumber: '71',
      Symbol: 'Lu',
      Name: 'Lutetium',
      AtomicWeight: '174.97',
      ElectronShell: [2, 8, 18, 32, 9, 2],
      Trend: 'lanthanoids'
    },
  ];

  Actinide: Element[] = [
    {
      AtomicNumber: '89',
      Symbol: 'Ac',
      Name: 'Actinium',
      AtomicWeight: '227',
      ElectronShell: [2, 8, 18, 32, 18, 9, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '90',
      Symbol: 'Th',
      Name: 'Thorium',
      AtomicWeight: '232.04',
      ElectronShell: [2, 8, 18, 32, 18, 10, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '91',
      Symbol: 'Pa',
      Name: 'Protactinium',
      AtomicWeight: '231.04',
      ElectronShell: [2, 8, 18, 32, 20, 9, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '92',
      Symbol: 'U',
      Name: 'Uranium',
      AtomicWeight: '238.03',
      ElectronShell: [2, 8, 18, 32, 21, 9, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '93',
      Symbol: 'Np',
      Name: 'Neptunium',
      AtomicWeight: '237',
      ElectronShell: [2, 8, 18, 32, 22, 9, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '94',
      Symbol: 'Pu',
      Name: 'Plutonium',
      AtomicWeight: '244',
      ElectronShell: [2, 8, 18, 32, 24, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '95',
      Symbol: 'Am',
      Name: 'Americium',
      AtomicWeight: '243',
      ElectronShell: [2, 8, 18, 32, 25, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '96',
      Symbol: 'Cm',
      Name: 'Curium',
      AtomicWeight: '247',
      ElectronShell: [2, 8, 18, 32, 25, 9, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '97',
      Symbol: 'Bk',
      Name: 'Berklium',
      AtomicWeight: '247',
      ElectronShell: [2, 8, 18, 32, 27, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '98',
      Symbol: 'Cf',
      Name: 'Califomium',
      AtomicWeight: '247',
      ElectronShell: [2, 8, 18, 32, 28, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '99',
      Symbol: 'Es',
      Name: 'Einsteinium',
      AtomicWeight: '252',
      ElectronShell: [2, 8, 18, 32, 29, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '100',
      Symbol: 'Fm',
      Name: 'Fermium',
      AtomicWeight: '257',
      ElectronShell: [2, 8, 18, 32, 30, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '101',
      Symbol: 'Md',
      Name: 'Mendelevium',
      AtomicWeight: '258',
      ElectronShell: [2, 8, 18, 32, 31, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '102',
      Symbol: 'No',
      Name: 'Noblium',
      AtomicWeight: '259',
      ElectronShell: [2, 8, 18, 32, 32, 8, 2],
      Trend: 'actinide'
    },
    {
      AtomicNumber: '103',
      Symbol: 'Lr',
      Name: 'Lawrencium',
      AtomicWeight: '259',
      ElectronShell: [2, 8, 18, 32, 32, 8, 3],
      Trend: 'actinide'
    },
  ];
  constructor() { }
}
