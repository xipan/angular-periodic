import { TestBed } from '@angular/core/testing';

import { ElementlistService } from './elementlist.service';

describe('ElementlistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ElementlistService = TestBed.get(ElementlistService);
    expect(service).toBeTruthy();
  });
});
