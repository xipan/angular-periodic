export class Element {
  constructor(
    public AtomicNumber : string,
    public Symbol : string,
    public Name : string,
    public AtomicWeight: string,
    public ElectronShell: Array<number>,
    public Trend  : string
  ) { }
}
