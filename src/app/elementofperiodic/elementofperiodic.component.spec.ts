import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementofperiodicComponent } from './elementofperiodic.component';

describe('ElementofperiodicComponent', () => {
  let component: ElementofperiodicComponent;
  let fixture: ComponentFixture<ElementofperiodicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementofperiodicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementofperiodicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
